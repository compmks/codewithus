﻿(function (AjaxRequest, undefined) {
    AjaxRequest.Post = function (jsonData, url, successCallback, errorCallback) {
        $.ajax({
            url: url,
            type: "POST",
            data: jsonData,
            datatype: "json",
            contentType: "application/json charset=utf-8",
            success: function (data) {
                successCallback(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (errorCallback) {
                    errorCallback();
                }
            }
        });
    };


    AjaxRequest.Get = function (jsonData, url, successCallback, errorCallback) {
        $.ajax({
            url: url,
            type: "GET",
            data: jsonData,
            datatype: "json",
            contentType: "application/json charset=utf-8",
            success: function (data) {
                    successCallback(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (errorCallback) {
                    errorCallback();
                }
            }

        });
    };

}(window.AjaxRequest = window.AjaxRequest || {}));