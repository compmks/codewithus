﻿ 
/*global AjaxRequest:false */

var Controllers = {
	relativePath: "",
	SetRelativePath : function(relPath){
		/// <summary>Sets the relative path for Ajax calls to the controller actions</summary>
		/// <param name="relPath" type="String">The relative path that is prefixed before the controller action URL</param>
		Controllers.relativePath = relPath;
	},
	'Home' :
	{
		'IndexURL' : 'Home/Index',
		'CapitalizeTextURL' : 'Home/CapitalizeText',
		'CapitalizeTextPost' : function(textJSON, successCallback, errorCallback) {
			/// <summary>HTTP Post request Ajax wrapper for the CapitalizeText action</summary>
			/// <param name="textJSON" type="String" >The data for the String parameter serialized as JSON</param>
			/// <param name="successCallback" >Optional,function to be run upon success of the call</param>
			/// <param name="errorCallback" >Optional, function to be run there was an error with the call</param>
			AjaxRequest.Post(textJSON,  Controllers.relativePath + Controllers.Home.CapitalizeTextURL, successCallback, errorCallback);
		}},
};

